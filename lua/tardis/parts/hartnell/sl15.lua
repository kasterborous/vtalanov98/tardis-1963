local PART={}
PART.ID = "hartnellsl15"
PART.Name = "1963 TARDIS Small Lever 15"
PART.Model = "models/doctorwho1200/hartnell/smalllever15.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/smalllever.wav"

TARDIS:AddPart(PART)