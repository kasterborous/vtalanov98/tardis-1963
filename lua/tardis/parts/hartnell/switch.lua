local PART={}
PART.ID = "hartnellswitch"
PART.Name = "1963 TARDIS Switch"
PART.Model = "models/doctorwho1200/hartnell/switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/switch.wav"

TARDIS:AddPart(PART)