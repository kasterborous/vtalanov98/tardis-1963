local PART={}
PART.ID = "hartnellwhiteswitch"
PART.Name = "1963 TARDIS White Switch"
PART.Model = "models/doctorwho1200/hartnell/whiteswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/rotaryswitch.wav"

TARDIS:AddPart(PART)