local PART={}
PART.ID = "hartnellhandle7"
PART.Name = "1963 TARDIS Handle 7"
PART.Model = "models/doctorwho1200/hartnell/handle7.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hartnell/handle.wav"

TARDIS:AddPart(PART)