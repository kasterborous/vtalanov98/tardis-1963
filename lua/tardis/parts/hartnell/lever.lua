local PART={}
PART.ID = "hartnelllever"
PART.Name = "1963 TARDIS Dematerialization Lever"
PART.Model = "models/doctorwho1200/hartnell/lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5

PART.SoundOff = "doctorwho1200/hartnell/leveroff.wav"
PART.SoundOn = "doctorwho1200/hartnell/leveron.wav"

TARDIS:AddPart(PART)