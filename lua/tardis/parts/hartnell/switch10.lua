local PART={}
PART.ID = "hartnellswitch10"
PART.Name = "1963 TARDIS Switch 10"
PART.Model = "models/doctorwho1200/hartnell/switch10.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/switch.wav"

TARDIS:AddPart(PART)