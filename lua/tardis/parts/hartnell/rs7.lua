local PART={}
PART.ID = "hartnellrs7"
PART.Name = "1963 TARDIS Rotary Switch 7"
PART.Model = "models/doctorwho1200/hartnell/rotaryswitch7.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/rotaryswitch.wav"

TARDIS:AddPart(PART)