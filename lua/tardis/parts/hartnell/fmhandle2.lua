local PART={}
PART.ID = "hartnellfmhandle2"
PART.Name = "1963 TARDIS Food Machine Handle 2"
PART.Model = "models/doctorwho1200/hartnell/fmhandle2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 4

PART.Sound = "doctorwho1200/hartnell/foodmachinehandle.wav"
PART.SoundPos = Vector(0,280,47)

TARDIS:AddPart(PART)