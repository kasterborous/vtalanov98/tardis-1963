local PART={}
PART.ID = "hartnellswitch3"
PART.Name = "1963 TARDIS Switch 3"
PART.Model = "models/doctorwho1200/hartnell/switch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/switch.wav"

TARDIS:AddPart(PART)