local PART={}
PART.ID = "hartnellsl8"
PART.Name = "1963 TARDIS Small Lever 8"
PART.Model = "models/doctorwho1200/hartnell/smalllever8.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/smalllever.wav"

TARDIS:AddPart(PART)