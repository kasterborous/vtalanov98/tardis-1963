local PART={}
PART.ID = "hartnellrs9"
PART.Name = "1963 TARDIS Rotary Switch 9"
PART.Model = "models/doctorwho1200/hartnell/rotaryswitch9.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/rotaryswitch.wav"

TARDIS:AddPart(PART)