local PART={}
PART.ID = "hartnellfr"
PART.Name = "1963 TARDIS Fast Return Switch"
PART.Model = "models/doctorwho1200/hartnell/fastreturn.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

PART.Sound = "doctorwho1200/hartnell/button.wav"

TARDIS:AddPart(PART)