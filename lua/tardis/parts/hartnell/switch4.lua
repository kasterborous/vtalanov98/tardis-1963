local PART={}
PART.ID = "hartnellswitch4"
PART.Name = "1963 TARDIS Switch 4"
PART.Model = "models/doctorwho1200/hartnell/switch4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/switch.wav"

TARDIS:AddPart(PART)
