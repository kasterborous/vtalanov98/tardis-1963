local PART={}
PART.ID = "hartnellrs"
PART.Name = "1963 TARDIS Rotary Switch"
PART.Model = "models/doctorwho1200/hartnell/rotaryswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/rotaryswitch.wav"

TARDIS:AddPart(PART)