local PART={}
PART.ID = "hartnellrs2"
PART.Name = "1963 TARDIS Rotary Switch 2"
PART.Model = "models/doctorwho1200/hartnell/rotaryswitch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/rotaryswitch.wav"

TARDIS:AddPart(PART)