local PART={}
PART.ID = "hartnellhandle"
PART.Name = "1963 TARDIS Handle"
PART.Model = "models/doctorwho1200/hartnell/handle.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hartnell/handle.wav"

TARDIS:AddPart(PART)