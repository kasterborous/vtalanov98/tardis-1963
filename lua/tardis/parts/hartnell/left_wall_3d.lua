local PART={}
PART.ID = "hartnell_left_wall_3d"
PART.Name = "Hartnell TARDIS 3D Left Wall"
PART.Model = "models/doctorwho1200/hartnell/1963walls3dalt.mdl"
PART.Collision = true
PART.AutoSetup = true

TARDIS:AddPart(PART)