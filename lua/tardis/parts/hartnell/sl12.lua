local PART={}
PART.ID = "hartnellsl12"
PART.Name = "1963 TARDIS Small Lever 12"
PART.Model = "models/doctorwho1200/hartnell/smalllever12.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/smalllever.wav"

TARDIS:AddPart(PART)