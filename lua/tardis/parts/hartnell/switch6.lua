local PART={}
PART.ID = "hartnellswitch6"
PART.Name = "1963 TARDIS Switch 6"
PART.Model = "models/doctorwho1200/hartnell/switch6.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/switch.wav"

TARDIS:AddPart(PART)