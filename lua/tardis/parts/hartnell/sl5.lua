local PART={}
PART.ID = "hartnellsl5"
PART.Name = "1963 TARDIS Small Lever 5"
PART.Model = "models/doctorwho1200/hartnell/smalllever5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/smalllever.wav"

TARDIS:AddPart(PART)