local PART={}
PART.ID = "hartnellstasisswitch"
PART.Name = "1963 TARDIS Stasis Switch"
PART.Model = "models/doctorwho1200/hartnell/stasisswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hartnell/rotaryswitch.wav"

TARDIS:AddPart(PART)