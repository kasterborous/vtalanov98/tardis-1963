local PART={}
PART.ID = "hartnellswitch11"
PART.Name = "1963 TARDIS Switch 11"
PART.Model = "models/doctorwho1200/hartnell/switch11.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/switch.wav"

TARDIS:AddPart(PART)