local PART={}
PART.ID = "hartnellhandle4"
PART.Name = "1963 TARDIS Handle 4"
PART.Model = "models/doctorwho1200/hartnell/handle4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hartnell/handle.wav"

TARDIS:AddPart(PART)