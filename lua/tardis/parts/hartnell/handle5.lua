local PART={}
PART.ID = "hartnellhandle5"
PART.Name = "1963 TARDIS Handle 5"
PART.Model = "models/doctorwho1200/hartnell/handle5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hartnell/handle.wav"

TARDIS:AddPart(PART)