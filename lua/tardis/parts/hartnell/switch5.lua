local PART={}
PART.ID = "hartnellswitch5"
PART.Name = "1963 TARDIS Switch 5"
PART.Model = "models/doctorwho1200/hartnell/switch5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hartnell/switch.wav"

TARDIS:AddPart(PART)