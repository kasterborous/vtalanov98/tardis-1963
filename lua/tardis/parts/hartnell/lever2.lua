local PART={}
PART.ID = "hartnelllever2"
PART.Name = "1963 TARDIS Lever 2"
PART.Model = "models/doctorwho1200/hartnell/lever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5

PART.SoundOff = "doctorwho1200/hartnell/leveroff.wav"
PART.SoundOn = "doctorwho1200/hartnell/leveron.wav"

TARDIS:AddPart(PART)