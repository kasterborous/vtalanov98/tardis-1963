local PART={}
PART.ID = "hartnellhandle6"
PART.Name = "1963 TARDIS Handle 6"
PART.Model = "models/doctorwho1200/hartnell/handle6.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hartnell/handle.wav"

TARDIS:AddPart(PART)