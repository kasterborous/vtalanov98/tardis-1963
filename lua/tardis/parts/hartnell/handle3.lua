local PART={}
PART.ID = "hartnellhandle3"
PART.Name = "1963 TARDIS Handle 3"
PART.Model = "models/doctorwho1200/hartnell/handle3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hartnell/handle.wav"

TARDIS:AddPart(PART)